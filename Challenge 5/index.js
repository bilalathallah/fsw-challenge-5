// index.js
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const multer = require('multer');
const handler = require('./controller/carController');
const path = require('path');

app.set("view engine", "ejs");

app.use(express.json());
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// handle storage using multer
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, 'public/images');
  },
  filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  }
})

let upload = multer({ storage: storage });

// GET all car
app.get("/", handler.listCar);

// Create a car
app.get("/addcar", handler.addCarView);
app.post("/car", upload.single('inputFoto'),  handler.addCar);

// Update a car
app.post("/update/:id", handler.editCarView);
app.post("/updated/:id", upload.single("inputFoto"), handler.editCar);

// Delete a car
app.post("/delete/:id", handler.deleteCar);

// listen on port
app.listen(5000, () => console.log("Server running at http://localhost:5000"));
